package com.framifykit.logistics.api.executor.config;


/**
 * <p>
 * 物流组件获取配置接口
 * </p>
 *
 * @author fxn
 * @since
 **/
public interface ILogisticsGetConfig<CONFIG> {

    /**
     * 获取配置类
     *
     * @return
     */
    CONFIG getApiConfig();
}
