package com.framifykit.logistics.kd100;


import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.ReflectUtil;
import com.alibaba.fastjson.JSONObject;
import com.famifykit.starter.common.exception.ServiceException;
import com.famifykit.starter.common.result.FramifyResult;

import com.framifykit.logistics.api.enums.LogisticsTypeEnum;
import com.framifykit.logistics.api.executor.AbstractLogisticsExecutor;
import com.framifykit.logistics.api.executor.ILogisticsExecutor;
import com.framifykit.logistics.api.executor.config.ILogisticsGetConfig;
import com.framifykit.logistics.api.executor.factory.LogisticsExecutorFactory;
import com.framifykit.logistics.kd100.client.DefaultKD100Client;
import com.framifykit.logistics.kd100.config.KD100ApiConfig;
import com.framifykit.logistics.kd100.config.KD100ApiConfigKit;
import com.framifykit.logistics.kd100.domain.req.KD100Req;
import com.framifykit.logistics.kd100.domain.res.*;
import com.framifykit.logistics.kd100.util.SignUtils;
import com.framifykit.logistics.kd100.domain.res.dto.KD100CheckAddressResParam;
import com.framifykit.logistics.kd100.domain.res.dto.KD100QueryCapacityResParam;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import static com.framifykit.logistics.api.constant.LogisticsErrorCodeConstants.THIRD_PARTY_API_ERROR;
import static com.framifykit.logistics.kd100.constant.KD100ApiConstant.*;


/**
 * <p>
 * 调用快递100接口执行器
 * </p>
 *
 * @author fxn
 * @since 1.0.0
 **/
@Slf4j
public class KD100Executor extends AbstractLogisticsExecutor<KD100Req, KD100Res>
        implements ILogisticsExecutor<KD100Req, FramifyResult<KD100Res>>,
        ILogisticsGetConfig<KD100ApiConfig> {

    private KD100Executor() {
    }

    static {
        LogisticsExecutorFactory.register(LogisticsTypeEnum.KD_100, new KD100Executor());
    }


    @Override
    public FramifyResult<KD100Res> execute(KD100Req kd100Req) throws ServiceException {

        KD100Res kd100Res;
        try {
            String methodType = kd100Req.getMethodType();
            kd100Res = ReflectUtil.invoke(this, methodType, kd100Req);
        } catch (Exception e) {
            log.error("第三方物流平台:{}调用系统异常", LogisticsTypeEnum.KD_100.getName(), e);
            throw new ServiceException(THIRD_PARTY_API_ERROR);
        }

        return FramifyResult.success(kd100Res);
    }

    @Override
    protected KD100Res queryTrack(KD100Req kd100Req) throws ServiceException {

        DefaultKD100Client kd100Client = getApiConfig().getKD100Client();
        String result = kd100Client.execute(QUERY_URL, kd100Req.getQueryTrackReq());

        KD100QueryTrackRes KD100QueryTrackRes = JSONObject.parseObject(result, KD100QueryTrackRes.class);

        KD100Res kd100Res = KD100Res.builder()
                .queryTrackRes(KD100QueryTrackRes)
                .build();

        return kd100Res;
    }


    @Override
    protected KD100Res placeOrder(KD100Req kd100Req) throws ServiceException {

        DefaultKD100Client kd100Client = getApiConfig().getKD100Client();
        String result = kd100Client.execute(B_ORDER_URL, kd100Req.getPlaceOrderReq());

        KD100PlaceOrderRes kd100PlaceOrderRes = JSONObject.parseObject(result, KD100PlaceOrderRes.class);

        KD100Res kd100Res = KD100Res.builder()
                .placeOrderRes(kd100PlaceOrderRes)
                .build();

        return kd100Res;
    }

    @Override
    protected KD100Res cancelOrder(KD100Req kd100Req) throws ServiceException {

        DefaultKD100Client kd100Client = getApiConfig().getKD100Client();
        String result = kd100Client.execute(B_ORDER_URL, kd100Req.getPlaceOrderReq());

        KD100CancelOrderRes kd100CancelOrderRes = JSONObject.parseObject(result, KD100CancelOrderRes.class);

        KD100Res kd100Res = KD100Res.builder()
                .cancelOrderRes(kd100CancelOrderRes)
                .build();

        return kd100Res;
    }

    @Override
    protected KD100Res queryCost(KD100Req KD100Req) {
        return null;
    }

    @Override
    protected KD100Res subscribeOrder(KD100Req kd100Req) throws ServiceException {

        DefaultKD100Client kd100Client = getApiConfig().getKD100Client();
        String result = kd100Client.execute(SUBSCRIBE_URL, kd100Req.getSubscribeReq());

        KD100SubscribeRes kd100SubscribeRes = JSONObject.parseObject(result, KD100SubscribeRes.class);

        KD100Res kd100Res = KD100Res.builder()
                .subscribeRes(kd100SubscribeRes)
                .build();

        return kd100Res;
    }

    @Override
    protected KD100Res queryCapacity(KD100Req kd100Req) throws ServiceException {

        DefaultKD100Client kd100Client = getApiConfig().getKD100Client();
        String result = kd100Client.execute(QUERY_URL, kd100Req.getQueryCapacityReq());

        KD100QueryCapacityRes<KD100QueryCapacityResParam> KD100QueryCapacityRes = JSONObject
                .parseObject(result, new TypeReference<KD100QueryCapacityRes<KD100QueryCapacityResParam>>() {
                });

        KD100Res kd100Res = KD100Res.builder()
                .queryCapacityRes(KD100QueryCapacityRes)
                .build();

        return kd100Res;
    }

    @Override
    protected KD100Res getCode(KD100Req kd100Req) throws ServiceException {

        DefaultKD100Client kd100Client = getApiConfig().getKD100Client();
        String result = kd100Client.execute(QUERY_URL, kd100Req.getGetCodeReq());

        KD100GetCodeRes<Map<String, String>> KD100GetCodeRes = JSONObject
                .parseObject(result, new TypeReference<KD100GetCodeRes<Map<String, String>>>() {
                });

        KD100Res kd100Res = KD100Res.builder()
                .getCodeRes(KD100GetCodeRes)
                .build();

        return kd100Res;
    }

    @Override
    protected KD100Res checkAddress(KD100Req kd100Req) throws ServiceException {

        DefaultKD100Client kd100Client = getApiConfig().getKD100Client();
        String result = kd100Client.execute(REACHABLE_URL, kd100Req.getCheckAddressReq());

        KD100CheckAddressRes<KD100CheckAddressResParam> KD100GetCodeRes = JSONObject
                .parseObject(result, new TypeReference<KD100CheckAddressRes<KD100CheckAddressResParam>>() {
                });

        KD100Res kd100Res = KD100Res.builder()
                .checkAddressRes(KD100GetCodeRes)
                .build();

        return kd100Res;
    }

    @Override
    public KD100ApiConfig getApiConfig() {
        return KD100ApiConfigKit.getApiConfig();
    }

    @Override
    protected String createSign(String signSource) {
        return SignUtils.sign(signSource);
    }

    protected String createSign(String timeMillis, String signSource) {
        return SignUtils.sign(timeMillis + signSource);
    }
}


